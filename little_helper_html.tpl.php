<?php
/**
 * @file
 * HTML structure for the helper block.
 */
?>

<div id="little-helper-container">
  <?php print $cartoon_image; ?>
  <div id="little-helper-quote-container">
    <div id="quote-angle"></div>
    <span id="little-helper-quote">
      <?php print $helper_quote; ?>
    </span>
  </div>
</div>
