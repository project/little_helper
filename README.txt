The Little Helper module provides a block that will display to the user random 
messages that an admin specifies.

These messages can range from helpful tips to cute little sayings. The 
functionality of this module is similar to that of theMailChimp monkey.

Installation
------------
Follow the standard contributed module installation process:
http://drupal.org/documentation/install/modules-themes/modules-7


Requirements
------------
Dependencies include modules enabled by default.
- File
- Image
- Block

Use
---
1. Configure the cartoon image and tips of your helper at 
   admin/config/user-interface/little-helper.
2. Go to the block administration interface and look for the "My Little Helper" 
   block to configure any block related settings.

You can also configure access permissions for the 
admin/config/user-interface/little-helper page.

There are some starter styles within the module but these can be overridden 
with your theme's css.
